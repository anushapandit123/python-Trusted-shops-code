# cc-shop-profile-page

How would you further develop this test:

1. What kind of checks would you add to the current tests?
  - Check if all the elements are correctly loaded when the window is opened
  - Check the login functionality. 
  - Check Hamburger menu and it's functionality
  - Filter star rating and refresh the page. The same page should be displayed, and all the field should be in the clear or unfilled state. and so on.
  - I would try to add more reviews and check whether my test cases are working fine or not.

  I would store all the locators and test data in the separate file.
  I would add as much as comments as possible which is a best practice I believe.
  
2. What are the next tests you would recommend implementing?
   I would recommend doing Usability testing.
  
3. Does the profile page have non-functional properties? - How would you test these properties? 
   - All the elements, Text boxes, Star rating, and Buttons should be appropriately aligned for better usability.
   - The rating feature should work properly in all browsers and in all OS ( having GUI).
   - In terms of Page Performance, The rating feature should work properly when the internet speed is low, and a proper loading mask should be displayed.

# cc-distributed-system

1. Describe how you would approach the discovered issue. Consider the following questions:
    * How can your team guarantee tests are reliable?
	- If the data is changing frequently which leads to tests breaking regularly, The number '1234' keeps changing every time.(ex: id="xyz1234")
	  In this case, we need to use some other methods to identify the web element. 
	  Here are some methods that we can use to identify such dynamic web elements on our web page which helps to ensure our tests are reliable..
	  1. XPath, using Contains. example: //button[contains(@id,'xyz')]
	  2. Xpath, using starts-with.  example: //button[starts-with(@id, 'xyz')]
	  3. We can use multiple elements to locate an element. 
	  4. We can identify using index in case we have multiple entries.

    * How can all teams collaborate in ensuring interfaces are working properly?
	- In an agile testing, collaboration is the key to produce a quality product and it starts from the beginning of the process, by the stage of planning QA process, which includes test strategies, test plan, test cases 
	 and implementation of TDD, ATDD or BDD testing approaches is a good way to improve the quality of software. 
	- By monitoring all aspects of the agile testing process, such as
	  * Requirements Management
	  * User Story Management
	  * Test Case Management
 	  * Defect Management
	  * Test Script Repository
	    help multiple teams to collaborate and be more productive. 
	- In addition to this, every day the team do a meeting for less than 15 minutes and discuss on, 
	  What did we do yesterday? what will we do today? and Are there any obstacles/Blockers in our way. We will explain about our task breakdown. This is how we keep a track of our testing growth.
	  And by following good practices of testing such as 
	  * Add appropriate comments
	  * Maintain the test data in a separate file
	  * Identify the reusable methods and write it in a separate file
	  * Follow the language-specific coding conventions
	  * Run our scripts regularly
	  This is how we as a team collaborate in ensuring our interfaces/web pages are working properly with no critical bugs. 


Note: 1)In reference to the 3rd scenario(cc-shop-profile-page),
        Could not able to find 'info icon' in the review block, so skipped that scenario in the test suite.
	  2)For the 4th scenario(cc-shop-profile-page)
	    Please refer test_one_star.py file. I have tried it only for first review and I really want to know how you manage this in real time.



